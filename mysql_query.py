import mysql.connector
from mysql.connector import Error
from db_servers import db_config

########
###Variable declaration
########
###
#Show DB
###
show_db = "SHOW DATABASES"

###
#Create DB
###
db_names = ['test_db_6']
create_db = "CREATE DATABASE %s"
out_db = []

########
###Execute show tables
########
show_tb = "SELECT * FROM %s"

########
###Connect to db
########
try:
    print ("Connecting to:",db_config["mysql"]["host"])
    connect = mysql.connector.connect(
        host = (db_config["mysql"]["host"]),
        port = (db_config["mysql"]["port"]),
        user = (db_config["mysql"]["user"]),
        password = (db_config["mysql"]["password"]),
    )
    print ("Connection established:",db_config["mysql"]["host"])

########
###Execute command "show_db"
########
    try:
        with connect.cursor() as cursor:
            cursor.execute(show_db)
            rows = cursor.fetchall()

            for row in rows:
                for i in row:
                    out_db.append(i)
    except Error as e:
        print (e)

########
###Execute command "create_db"
########
    try:
        with connect.cursor() as cursor:
            for db_name in db_names:
                if db_name in out_db:
                    print ("Database", db_name, "exists.")
                else:
                    cursor.execute(create_db %db_name)
                    connect.commit()
                    print ("Database", db_name, "created.")
    except Error as e:
        print (e)

########
###Execute show tables
########
    try:
        with connect.cursor() as cursor:
            for show_tbs in out_db:
                cursor.execute(show_tb %show_tbs)
                rows = cursor.fetchall()

            for row in rows:
                for i in row:
                    print (i)


    except Error as e:
        print (e)            

########
###Close main connection
########
except Error as error:
    print ("Error conection:",error, db_config["mysql"]["host"])

########
###Close connection
########
finally:
    cursor.close()
    connect.close()
    print("Connection closed.")


########
###Show output db
########
def all_db():
    print ()
    print("All db's:")
    for i in out_db:
        print (i)

def main ():
    all_db ()

#main ()